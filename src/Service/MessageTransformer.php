<?php

namespace App\Service;

use App\Service\Telegram\Download;
use Axl1232\PhpDiscordWebhook\Message as DiscordMessage;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Exception\TelegramException;

class MessageTransformer
{
    private Download $download;

    public function __construct(Download $download)
    {
        $this->download = $download;
    }

    /**
     * @throws TelegramException
     */
    public function __invoke(Message $message): DiscordMessage
    {
        $discordMessage = new DiscordMessage();

        switch ($message->getType()) {
            case 'text':
                $discordMessage->setContent(trim($message->getText()));
                break;

            case 'video':
                $file = $this->getFile($message->getVideo()->getFileId());
                $discordMessage->setFile($file);

                $info = pathinfo($file);

                if (isset($info['extension']) && $info['extension'] === 'm4v') {
                    $discordMessage->setFilename(sprintf('%s.mp4', $info['filename']));
                }
                break;

            case 'animation':
                $file = $this->getFile($message->getAnimation()->getFileId());
                $discordMessage->setFile($file);
                //$discordEmbed->setVideo((new Embed\Video())->setUrl('attachment://' . basename($file)));
                break;

            case 'audio':
                $file = $this->getFile($message->getAudio()->getFileId());
                $discordMessage->setFile($file);
                break;

            case 'voice':
                $file = $this->getFile($message->getVoice()->getFileId());
                $discordMessage->setFile($file);
                break;

            case 'document':
                $file = $this->getFile($message->getDocument()->getFileId());
                $discordMessage->setFile($file);
                break;

            case 'photo':
                $images = $message->getPhoto();

                usort(
                    $images,
                    static fn($a, $b) => -1 * ($a->getFileSize() <=> $b->getFileSize())
                );

                $file = $this->getFile($images[0]->getFileId());
                $discordMessage->setFile($file);
                break;
        }

        return $discordMessage;
    }

    /**
     * @throws TelegramException
     */
    private function getFile(string $fileId): string
    {
        $path = ($this->download)($fileId);

        register_shutdown_function(fn ($path) => unlink($path), $path);

        return $path;
    }
}
