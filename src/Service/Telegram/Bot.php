<?php

namespace App\Service\Telegram;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Bot
{
    private Telegram $telegram;

    /**
     * @throws TelegramException
     */
    public function __construct(ParameterBagInterface $parameters)
    {
        $this->telegram = new Telegram($parameters->get('app.telegram.api_key'));
    }

    public function __invoke(): Telegram
    {
        return $this->telegram;
    }
}
