<?php

namespace App\Service\Telegram;

use App\Service\S3\Receive;
use App\Service\S3\Save;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Psr\Log\LoggerInterface;
use RuntimeException;

class Download
{
    private Telegram $telegram;
    private Save $save;
    private Receive $receive;
    private LoggerInterface $logger;

    public function __construct(Bot $bot, Save $save, Receive $receive)
    {
        $this->telegram = $bot();
        $this->save = $save;
        $this->receive = $receive;
    }

    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @throws TelegramException
     */
    public function __invoke(string $fileId): string
    {
        $dir = sys_get_temp_dir();
        $this->telegram->setDownloadPath($dir);

        $this->logger->info('Get telegram file info', ['id' => $fileId]);

        $response = Request::getFile(['file_id' => $fileId]);

        if (!$response->isOk()) {
            throw new RuntimeException('Unable to get file info');
        }

        $filename = $response->getResult()->getFilePath();
        $path = "{$dir}/{$filename}";

        if (($this->receive)($filename, $path)) {
            return $path;
        }

        $this->logger->info('Download telegram file', ['filePath' => $filename]);

        if (!Request::downloadFile($response->getResult())) {
            throw new RuntimeException('Unable to download file');
        }

        if (!($this->save)($filename, $path)) {
            throw new RuntimeException('Unable to save file to s3');
        }

        return $path;
    }
}
