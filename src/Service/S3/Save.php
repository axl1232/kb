<?php

namespace App\Service\S3;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Psr\Log\LoggerInterface;

class Save
{
    private string $bucket;
    private S3Client $s3;
    private LoggerInterface $logger;

    public function __construct(S3Client $s3)
    {
        $this->bucket = $_ENV['S3_BUCKET'] ?? '';
        $this->s3 = $s3;
    }

    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function __invoke(string $key, string $path): bool
    {
        try {
            $this->logger->info('Upload file to s3', ['bucket' => $this->bucket, 'key' => $key]);

            $this->s3->upload($this->bucket, $key, fopen($path, 'rb'));

            return true;
        } catch (S3Exception $e) {
            $this->logger->error('Unable to upload file to s3', ['bucket' => $this->bucket, 'key' => $key]);
        }

        return false;
    }
}
