<?php

namespace App\Controller;

use App\Exception\UnauthorizedException;
use App\Service\MessageTransformer;
use App\Service\Telegram\Bot;
use Axl1232\PhpDiscordWebhook\Exception\DiscordInvalidResponseException;
use Axl1232\PhpDiscordWebhook\Exception\DiscordSerializeException;
use Axl1232\PhpDiscordWebhook\Webhook;
use JsonException;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request as TelegramRequest;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class TelegramHookController extends AbstractController
{
    private LoggerInterface $logger;
    private MessageTransformer $messageTransformer;

    public function __construct(MessageTransformer $messageTransformer, Bot $_)
    {
        $this->messageTransformer = $messageTransformer;
    }

    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="index", methods={"POST"})
     *
     * @return Response
     */
    public function index(): Response
    {
        try {
            $input = TelegramRequest::getInput();
            $data = json_decode($input, true, 512, JSON_THROW_ON_ERROR);
            $update = new Update($data);
            $message = $update->getMessage() ?? $update->getCallbackQuery()->getMessage();

            $this->authorize($message->getFrom()->getId());

            switch ($update->getUpdateType()) {
                case Update::TYPE_CALLBACK_QUERY:
                    $this->processCallbackQuery($update, $message);
                    break;

                case Update::TYPE_MESSAGE:
                    $this->processMessage($update);
                    break;

                default:
                    $this->sendReply($message, 'Unknown message type');
            }
        } catch (TelegramException|JsonException|DiscordInvalidResponseException|DiscordSerializeException|UnauthorizedException $e) {
            $this->logError($update ?? null, $e->getMessage());
        } catch (Throwable $e) {
            $this->logError($update ?? null, $e->getMessage());
        }

        $this->logger->info('Done');

        return new Response();
    }

    /**
     * @throws UnauthorizedException
     */
    private function authorize(int $id): void
    {
        if (
            $id !== (int)explode(':', $this->getParameter('app.telegram.api_key'))[0]
            && !in_array($id, $this->getParameter('app.telegram.admin_ids'), false)
        ) {
            throw new UnauthorizedException('You can\'t use this bot. Go away.');
        }
    }

    /**
     * @throws DiscordSerializeException
     * @throws DiscordInvalidResponseException
     * @throws TelegramException
     * @throws JsonException
     */
    private function processCallbackQuery(Update $update, Message $message): void
    {
        $discordChannels = $this->getDiscordChannels();
        $data = $update->getCallbackQuery()->getData();

        if (!isset($discordChannels[$data])) {
            $this->logger->info('Received unknown callback query data', ['data' => $data]);

            throw new RuntimeException('Received unknown callback query data');
        }

        $this->logger->info('Received callback query', ['data' => $data]);
        $this->logger->info('Transform message for discord');

        $this->updateMessage($message, 'Building message...');
        $discordMessage = ($this->messageTransformer)($message->getReplyToMessage());

        $this->updateMessage($message, "Sending to #{$data}...");
        $this->logger->info('Send message to discord');

        (new Webhook($discordChannels[$data]))->send($discordMessage);
        $this->updateMessage($message, "Sent to #{$data}.");
    }

    private function updateMessage(Message $message, string $text): void
    {
        TelegramRequest::editMessageText([
            'chat_id' => $message->getChat()->getId(),
            'message_id' => $message->getMessageId(),
            'text' => $text,
            'reply_markup' => null,
        ]);
    }

    /**
     * @throws TelegramException
     */
    private function processMessage(Update $update): void
    {
        if (
            $update->getMessage()->getReplyToMessage() !== null
            || $update->getMessage()->getCommand() !== null
            || $update->getMessage()->getSticker() !== null
        ) {
            $this->logger->info(
                'Received wrong message type',
                [
                    'isReplyToMessage' => $update->getMessage()->getReplyToMessage() !== null,
                    'isCommand' => $update->getMessage()->getCommand() !== null,
                    'isSticker' => $update->getMessage()->getSticker() !== null,
                ]
            );

            throw new RuntimeException('Received wrong message type');
        }

        $this->logger->info('Received new message');

        TelegramRequest::sendMessage([
            'chat_id' => $update->getMessage()->getChat()->getId(),
            'reply_to_message_id' => $update->getMessage()->getMessageId(),
            'text' => 'Which channel to send the message to?',
            'reply_markup' => new InlineKeyboard(
                ...array_chunk(
                    array_map(
                        static fn(string $channel) => new InlineKeyboardButton([
                            'text' => "#{$channel}",
                            'callback_data' => $channel,
                        ]),
                        array_keys($this->getDiscordChannels())
                    ),
                    2
                )
            ),
        ]);
    }

    private function getDiscordChannels(): array
    {
        return $this->getParameter('app.discord_channels');
    }

    /**
     * @throws TelegramException
     */
    private function sendReply(Message $message, string $data): void
    {
        TelegramRequest::sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'reply_to_message_id' => $message->getMessageId(),
            'text' => $data,
        ]);
    }

    private function logError(?Update $update, string $error)
    {
        $this->logger->critical($error);

        $message = $update->getMessage() ?? $update->getCallbackQuery()->getMessage();

        try {
            if (empty($message)) {
                TelegramRequest::sendMessage(['chat_id' => $message->getChat()->getId(), 'text' => $error]);
            } else {
                $this->sendReply($message, $error);
            }
        } catch (Throwable $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
