<?php

namespace App\Command\Temp;

use App\Service\S3\Save;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MoveFilesToS3 extends Command
{
    protected static $defaultName = 'temp:moveFilesToS3';
    private Save $save;
    private LoggerInterface $logger;

    public function __construct(Save $save)
    {
        $this->save = $save;

        parent::__construct();
    }

    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }


    protected function configure(): void
    {
        $this->addArgument('path', InputArgument::REQUIRED, 'Path with images');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $path = $input->getArgument('path');

        if (!file_exists($path)) {
            $this->logger->error('Path does not exist', ['path' => $path]);

            return Command::FAILURE;
        }

        $dots = ['.', '..'];

        foreach (scandir($path) as $dir) {
            if (in_array($dir, $dots)) {
                continue;
            }

            foreach (scandir("{$path}/{$dir}") as $fileType) {
                if (in_array($fileType, $dots)) {
                    continue;
                }

                foreach (scandir("{$path}/{$dir}/{$fileType}") as $file) {
                    if (in_array($file, $dots)) {
                        continue;
                    }

                    ($this->save)("{$fileType}/{$file}", "{$path}/{$dir}/{$fileType}/{$file}");
                }
            }
        }

        return Command::SUCCESS;
    }
}
