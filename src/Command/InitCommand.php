<?php

namespace App\Command;

use App\Service\Telegram\Bot;
use Longman\TelegramBot\Telegram;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Throwable;

class InitCommand extends Command
{
    protected static $defaultName = 'init';

    private ParameterBagInterface $parameterBag;
    private Telegram $bot;
    private LoggerInterface $logger;

    public function __construct(ParameterBagInterface $parameterBag, Bot $bot)
    {
        $this->parameterBag = $parameterBag;
        $this->bot = $bot();

        parent::__construct();
    }

    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $webhook = $this->parameterBag->get('app.telegram.webhook_url');

            $this->logger->info('Setting telegram webhook', ['webhook' => $webhook]);

            $this->bot->setWebhook($webhook);

            return Command::SUCCESS;
        } catch (Throwable $e) {
            $this->logger->error('Unable to set telegram webhook', ['error' => $e->getMessage()]);

            return Command::FAILURE;
        }
    }
}
