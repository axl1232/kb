.PHONY: up down

ARCH = $(shell /usr/bin/arch)
KB_ENV ?= dev

ifeq ("$(ARCH)", "armv7l")
    SUFFIX=.arm32v7
else ifeq ("$(ARCH)", "aarch64")
    SUFFIX=.arm32v7
endif

up:
	echo "APP_ENV=$(KB_ENV)" > .env.local
	SUFFIX="${SUFFIX}" docker compose -f docker-compose.yml up -d
down:
	SUFFIX="${SUFFIX}" docker compose down
