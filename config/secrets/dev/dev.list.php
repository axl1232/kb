<?php

return [
    'AWS_KEY' => null,
    'AWS_SECRET' => null,
    'DISCORD_CHANNELS' => null,
    'S3_BUCKET' => null,
    'TELEGRAM_ADMIN_IDS' => null,
    'TELEGRAM_BOT_API_KEY' => null,
    'TELEGRAM_WEBHOOK_URL' => null,
];
